import Vue from 'vue';
import Vuex from 'vuex';
import {
  setData,
  resultField,
  newLotteryField,
  listField
} from '@/helper/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    config: {
      name: '尾牙抽獎',
      number: 12,
      firstPrize: 10
    },
    result: {
      firstPrize: []
    },
    newLottery: [],
    list: [
      { key: 1, name: '劉惠卿' },
      { key: 2, name: '邱伃廷' },
      { key: 3, name: '李宜樺' },
      { key: 4, name: '吳采玲' },
      { key: 5, name: '陳秉鈞' },
      { key: 6, name: '歐怡廷' },
      { key: 7, name: '張伶憶' },
      { key: 8, name: '謝沂臻' },
      { key: 9, name: '賴德福' },
      { key: 10, name: '趙國綱' },
      { key: 11, name: '馬郁涵' },
      { key: 12, name: '劉沅沅' }
    ],
    photos: [
      {
        createdTime: '1641968058540',
        id: 1,
        updateTime: '1641968058540',
        value: 'img/劉惠卿.png'
      },
      {
        createdTime: '1641968058540',
        id: 2,
        updateTime: '1641968058540',
        value: 'img/邱伃廷.png'
      },
      {
        createdTime: '1641968058540',
        id: 3,
        updateTime: '1641968058540',
        value: 'img/李宜樺.png'
      },
      {
        createdTime: '1641968058540',
        id: 4,
        updateTime: '1641968058540',
        value: 'img/吳采玲.png'
      },
      {
        createdTime: '1641968058540',
        id: 5,
        updateTime: '1641968058540',
        value: 'img/陳秉鈞.png'
      },
      {
        createdTime: '1641968058540',
        id: 6,
        updateTime: '1641968058540',
        value: 'img/歐怡廷.png'
      },
      {
        createdTime: '1641968058540',
        id: 7,
        updateTime: '1641968058540',
        value: 'img/張伶憶.png'
      },
      {
        createdTime: '1641968058540',
        id: 8,
        updateTime: '1641968058540',
        value: 'img/謝沂臻.png'
      },
      {
        createdTime: '1641968058540',
        id: 9,
        updateTime: '1641968058540',
        value: 'img/賴德福.png'
      },
      {
        createdTime: '1641968058540',
        id: 10,
        updateTime: '1641968058540',
        value: 'img/趙國綱.png'
      },
      {
        createdTime: '1641968058540',
        id: 11,
        updateTime: '1641968058540',
        value: 'img/馬郁涵.png'
      },
      {
        createdTime: '1641968058540',
        id: 12,
        updateTime: '1641968058540',
        value: 'img/劉沅沅.png'
      }
    ]
  },
  mutations: {
    setClearConfig(state) {
      state.config = {
        name: '尾牙抽獎',
        number: 12,
        firstPrize: 10
      };
      state.newLottery = [];
    },
    setClearList(state) {
      state.list = [
        { key: 1, name: '劉惠卿' },
        { key: 2, name: '邱伃廷' },
        { key: 3, name: '李宜樺' },
        { key: 4, name: '吳采玲' },
        { key: 5, name: '陳秉鈞' },
        { key: 6, name: '歐怡廷' },
        { key: 7, name: '張伶憶' },
        { key: 8, name: '謝沂臻' },
        { key: 9, name: '賴德福' },
        { key: 10, name: '趙國綱' },
        { key: 11, name: '馬郁涵' },
        { key: 12, name: '劉沅沅' }
      ];
    },
    setClearPhotos(state) {
      state.photos = [
        {
          createdTime: '1641968058540',
          id: 1,
          updateTime: '1641968058540',
          value: 'img/劉惠卿.png'
        },
        {
          createdTime: '1641968058540',
          id: 2,
          updateTime: '1641968058540',
          value: 'img/邱伃廷.png'
        },
        {
          createdTime: '1641968058540',
          id: 3,
          updateTime: '1641968058540',
          value: 'img/李宜樺.png'
        },
        {
          createdTime: '1641968058540',
          id: 4,
          updateTime: '1641968058540',
          value: 'img/吳采玲.png'
        },
        {
          createdTime: '1641968058540',
          id: 5,
          updateTime: '1641968058540',
          value: 'img/陳秉鈞.png'
        },
        {
          createdTime: '1641968058540',
          id: 6,
          updateTime: '1641968058540',
          value: 'img/歐怡廷.png'
        },
        {
          createdTime: '1641968058540',
          id: 7,
          updateTime: '1641968058540',
          value: 'img/張伶憶.png'
        },
        {
          createdTime: '1641968058540',
          id: 8,
          updateTime: '1641968058540',
          value: 'img/謝沂臻.png'
        },
        {
          createdTime: '1641968058540',
          id: 9,
          updateTime: '1641968058540',
          value: 'img/賴德福.png'
        },
        {
          createdTime: '1641968058540',
          id: 10,
          updateTime: '1641968058540',
          value: 'img/趙國綱.png'
        },
        {
          createdTime: '1641968058540',
          id: 11,
          updateTime: '1641968058540',
          value: 'img/馬郁涵.png'
        },
        {
          createdTime: '1641968058540',
          id: 12,
          updateTime: '1641968058540',
          value: 'img/劉沅沅.png'
        }
      ];
    },
    setClearResult(state) {
      state.result = {
        firstPrize: []
      };
    },
    setClearStore(state) {
      state.config = {
        name: '尾牙抽獎',
        number: 12,
        firstPrize: 10
      };
      state.result = {
        firstPrize: []
      };
      state.newLottery = [];
      state.list = [
        { key: 1, name: '劉惠卿' },
        { key: 2, name: '邱伃廷' },
        { key: 3, name: '李宜樺' },
        { key: 4, name: '吳采玲' },
        { key: 5, name: '陳秉鈞' },
        { key: 6, name: '歐怡廷' },
        { key: 7, name: '張伶憶' },
        { key: 8, name: '謝沂臻' },
        { key: 9, name: '賴德福' },
        { key: 10, name: '趙國綱' },
        { key: 11, name: '馬郁涵' },
        { key: 12, name: '劉沅沅' }
      ];
      state.photos = [
        {
          createdTime: '1641968058540',
          id: 1,
          updateTime: '1641968058540',
          value: 'img/劉惠卿.png'
        },
        {
          createdTime: '1641968058540',
          id: 2,
          updateTime: '1641968058540',
          value: 'img/邱伃廷.png'
        },
        {
          createdTime: '1641968058540',
          id: 3,
          updateTime: '1641968058540',
          value: 'img/李宜樺.png'
        },
        {
          createdTime: '1641968058540',
          id: 4,
          updateTime: '1641968058540',
          value: 'img/吳采玲.png'
        },
        {
          createdTime: '1641968058540',
          id: 5,
          updateTime: '1641968058540',
          value: 'img/陳秉鈞.png'
        },
        {
          createdTime: '1641968058540',
          id: 6,
          updateTime: '1641968058540',
          value: 'img/歐怡廷.png'
        },
        {
          createdTime: '1641968058540',
          id: 7,
          updateTime: '1641968058540',
          value: 'img/張伶憶.png'
        },
        {
          createdTime: '1641968058540',
          id: 8,
          updateTime: '1641968058540',
          value: 'img/謝沂臻.png'
        },
        {
          createdTime: '1641968058540',
          id: 9,
          updateTime: '1641968058540',
          value: 'img/賴德福.png'
        },
        {
          createdTime: '1641968058540',
          id: 10,
          updateTime: '1641968058540',
          value: 'img/趙國綱.png'
        },
        {
          createdTime: '1641968058540',
          id: 11,
          updateTime: '1641968058540',
          value: 'img/馬郁涵.png'
        },
        {
          createdTime: '1641968058540',
          id: 12,
          updateTime: '1641968058540',
          value: 'img/劉沅沅.png'
        }
      ];
    },
    setConfig(state, config) {
      state.config = config;
    },
    setResult(state, result = {}) {
      state.result = result;

      setData(resultField, state.result);
    },
    setNewLottery(state, newLottery) {
      if (state.newLottery.find(item => item.name === newLottery.name)) {
        return;
      }
      state.newLottery.push(newLottery);
      setData(newLotteryField, state.newLottery);
    },
    setList(state, list) {
      const arr = state.list;
      list.forEach(item => {
        const arrIndex = arr.findIndex(data => data.key === item.key);
        if (arrIndex > -1) {
          arr[arrIndex].name = item.name;
        } else {
          arr.push(item);
        }
      });
      state.list = arr;

      setData(listField, arr);
    },
    setPhotos(state, photos) {
      state.photos = photos;
    }
  },
  actions: {},
  modules: {}
});
